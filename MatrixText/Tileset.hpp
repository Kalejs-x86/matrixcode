#pragma once
#include <SFML/Graphics.hpp>
#include <experimental/filesystem>

#define MT_TILESET_COUNT 16
#define MT_TILESIZE 10

namespace fs = std::experimental::filesystem;

class CTileSet
{
public:
	bool LoadTileSet(const fs::path& FilePath);
	int GetTileCount() const;
	sf::Sprite GetSprite(unsigned nTilePos) const;

private:
	bool m_bTileSetLoaded;
	sf::Texture m_TilesetTexture;
	unsigned m_nTileSize;
	// Texture size in pixels
	unsigned m_nTextureWidth, m_nTextureHeight;
	// Width and height in tiles (maximum tiles on x/y axis)
	unsigned m_nWidthTiles, m_nHeightTiles;

	sf::IntRect TranslatePosition(unsigned nTilePos) const;
};

class CTile
{
public:
	CTile(sf::Sprite Sprite, const sf::Vector2u& Pos, const sf::Color& StartingColor);

	sf::Vector2u GetPosition() const;
	sf::Vector2f GetScreenPosition() const;
	bool IsVisible();
	std::unique_ptr<sf::Sprite> GetSprite();

	void Update();
private:
	static const unsigned m_nColorGranularity = 5;
	sf::Color m_StartingColor;
	sf::Color m_TargetColor;
	unsigned m_nLifeTime;
	sf::Sprite m_Sprite;
	sf::Vector2u m_Position;
	sf::Color m_Color;
};