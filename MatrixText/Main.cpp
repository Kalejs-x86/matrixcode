#include <SFML/Graphics.hpp>
#include "MatrixCode.hpp"

#define MT_WINDOW_TILES 50
#define MT_WINDOW_WIDTH MT_TILESIZE * MT_WINDOW_TILES
#define MT_WINDOW_HEIGHT MT_TILESIZE* MT_WINDOW_TILES

int main(int argc, char** argv)
{
	sf::RenderWindow SF_Window(sf::VideoMode(MT_WINDOW_WIDTH, MT_WINDOW_HEIGHT), "The Matrix");
	SF_Window.setFramerateLimit(12);

	CTileSet TileSet;
	TileSet.LoadTileSet("res\\Matrix_10x10.bmp");

	CMatrixCode Matrix(&TileSet, sf::Vector2u(MT_WINDOW_TILES, MT_WINDOW_TILES));

	while (SF_Window.isOpen())
	{
		sf::Event SF_Event;
		while (SF_Window.pollEvent(SF_Event))
		{
			if (SF_Event.type == sf::Event::Closed)
				SF_Window.close();
		}

		SF_Window.clear(sf::Color::Black);
		//Draw here
		Matrix.Draw(SF_Window);
		SF_Window.display();
		Matrix.Update();
	}

	return 0;
}