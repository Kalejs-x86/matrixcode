#include "Tileset.hpp"
#include "Exception.hpp"


bool CTileSet::LoadTileSet(const fs::path& FilePath)
{
	try
	{
		sf::Image TilesetImage;
		CHECK(TilesetImage.loadFromFile(FilePath.u8string()), "Failed to open tileset file");
		TilesetImage.createMaskFromColor(sf::Color::Magenta);
		CHECK(m_TilesetTexture.loadFromImage(TilesetImage), "Failed to read in image as texture");

		sf::Vector2u TexSize = m_TilesetTexture.getSize();
		m_nTextureWidth = TexSize.x;
		m_nTextureHeight = TexSize.y;
		m_nTileSize = m_nTextureWidth / MT_TILESET_COUNT;

		m_nWidthTiles = m_nTextureWidth / m_nTileSize;
		m_nHeightTiles = m_nTextureHeight / m_nTileSize;

		m_bTileSetLoaded = true;
		return true;
	}
	catch (const std::exception& Ex)
	{
		m_bTileSetLoaded = false;
		return false;
	}
}

int CTileSet::GetTileCount() const
{
	if (m_nWidthTiles == 0 || m_nHeightTiles == 0)
		return -1;
	else
		return int(m_nWidthTiles) * int(m_nHeightTiles);
}

sf::Sprite CTileSet::GetSprite(unsigned nTilePos) const
{
	CHECK(m_bTileSetLoaded, "Tileset not loaded");

	sf::Sprite TileSprite;
	TileSprite.setTexture(m_TilesetTexture);
	TileSprite.setTextureRect(TranslatePosition(nTilePos));

	return TileSprite;
}

sf::IntRect CTileSet::TranslatePosition(unsigned nTilePos) const
{
	sf::IntRect Rect;
	Rect.height = Rect.width = m_nTileSize;
	// Assuming first tile is 0 (like an array)
	unsigned nTileY = nTilePos / m_nWidthTiles;
	unsigned nTileX = 0;
	if (nTileY == 0)
		nTileX = nTilePos;
	else
		nTileX = nTilePos % m_nWidthTiles;
	Rect.left = nTileX * m_nTileSize;
	Rect.top = nTileY * m_nTileSize;
	return Rect;
}

// class CTile

CTile::CTile(sf::Sprite Sprite, const sf::Vector2u& Pos, const sf::Color& StartingColor)
	: m_nLifeTime(0),
	m_Sprite(Sprite),
	m_Position(Pos),
	m_Color(sf::Color::White),
	m_StartingColor(StartingColor),
	m_TargetColor(0, 0, 0, 255)
{
}

sf::Vector2u CTile::GetPosition() const
{
	return m_Position;
}

sf::Vector2f CTile::GetScreenPosition() const
{
	return sf::Vector2f(float(m_Position.x * MT_TILESIZE), float(m_Position.y * MT_TILESIZE));
}

bool CTile::IsVisible()
{
	return m_Color.a != 0 && m_Color != sf::Color::Black;
}

std::unique_ptr<sf::Sprite> CTile::GetSprite()
{
	m_Sprite.setPosition(GetScreenPosition());
	m_Sprite.setColor(m_Color);
	return std::make_unique<sf::Sprite>(m_Sprite);
}

void CTile::Update()
{
	if (m_nLifeTime == 0)
		m_Color = m_StartingColor;

	auto fProgressColVal = [this](sf::Uint8& Src, const sf::Uint8& Target)
	{
		if (Src != Target)
		{
			if (abs(Src - Target) > m_nColorGranularity)
			{
				if (Src < Target)
					Src += m_nColorGranularity;
				else
					Src -= m_nColorGranularity;
			}
			else
				Src = Target;
		}
	};

	fProgressColVal(m_Color.r, m_TargetColor.r);
	fProgressColVal(m_Color.g, m_TargetColor.g);
	fProgressColVal(m_Color.b, m_TargetColor.b);
	fProgressColVal(m_Color.a, m_TargetColor.a);
	++m_nLifeTime;
}