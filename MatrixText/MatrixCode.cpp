#include "MatrixCode.hpp"

static std::random_device RandDevice;  //Will be used to obtain a seed for the random number engine

CMatrixCode::CMatrixCode(CTileSet* pTileSet, sf::Vector2u WindowLimits)
	: m_pTileSet(pTileSet),
	m_WindowLimits(WindowLimits),
	m_nFramesPassed(0)
{

}

void CMatrixCode::Update()
{
	auto TilesIt = m_ActiveTiles.begin();
	while (TilesIt != m_ActiveTiles.end())
	{
		std::shared_ptr<CTile> pTile = TilesIt->second;
		if (!pTile->IsVisible())
		{
			TilesIt = m_ActiveTiles.erase(TilesIt);
			continue;
		}
		else
		{
			pTile->Update();
			++TilesIt;
		}
	}

	auto DropperIt = m_TileDroppers.begin();
	while (DropperIt != m_TileDroppers.end())
	{
		CreateTile(*DropperIt);

		DropperIt->Position.y++;
		DropperIt->nLifeTime++;
		if (IsOutOfBounds(DropperIt->Position) || DropperIt->nLifeTime >= DropperIt->nMaxLifeTime)
		{
			DropperIt = m_TileDroppers.erase(DropperIt);
			continue;
		}
		else
			DropperIt++;
	}

	// Create some new droppers
	for (int i = 0; i < RandomNumber(1, 4); ++i)
	{
		auto DropperPos = RandomPosition(m_WindowLimits.x, m_WindowLimits.y);
		if (i < 2)
			DropperPos.y = 0;
		CreateDropper(DropperPos, RandomNumber(4, m_WindowLimits.y));
	}

	++m_nFramesPassed;
}

void CMatrixCode::Draw(sf::RenderWindow& Window) const
{
	for (const auto& Tile : m_ActiveTiles)
	{
		Window.draw(*Tile.second->GetSprite());
	}
}

sf::Vector2u CMatrixCode::RandomPosition(const unsigned nMaxX, const unsigned nMaxY) const
{
	std::mt19937 gen(RandDevice());
	std::uniform_int_distribution<> XPos(0, nMaxX);
	std::uniform_int_distribution<> YPos(0, nMaxY);

	return sf::Vector2u(XPos(gen), YPos(gen));
}

unsigned CMatrixCode::RandomNumber(const unsigned nMin, const unsigned nMax) const
{
	std::mt19937 gen(RandDevice());
	std::uniform_int_distribution<> RNum(nMin, nMax);
	return RNum(gen);
}

sf::Color CMatrixCode::RandomColor() const
{
/*
// True random but looks worse :(

#define RANDBYTE() RandomNumber(0, 255)
	return sf::Color(RANDBYTE(), RANDBYTE(), RANDBYTE(), 255);
#undef RANDBYTE
*/
	static const std::vector<sf::Color> RainbowColors = {
		{255, 0, 0, 255},
		{255, 127, 0, 255},
		{255, 255, 0, 255},
		{0, 255, 0, 255},
		{0, 0, 255, 255},
		{46, 43, 95, 255},
		{139, 0, 255, 255}
	};

	return RainbowColors[RandomNumber(0, 6)];
}

void CMatrixCode::CreateTile(const TileDropper& Dropper)
{
	auto Sprite = m_pTileSet->GetSprite(RandomNumber(0, m_pTileSet->GetTileCount() - 1));
	m_ActiveTiles[Dropper.Position] = std::make_shared<CTile>(Sprite, Dropper.Position, Dropper.TrailColor);
}

void CMatrixCode::CreateDropper(sf::Vector2u Pos, unsigned nLifeTime)
{
	TileDropper Dropper;
	Dropper.Position = Pos;
	Dropper.nMaxLifeTime = nLifeTime;
	Dropper.nLifeTime = 0;
#if MT_RANDOMCOLORS
	Dropper.TrailColor = RandomColor();
#else
	Dropper.TrailColor = sf::Color::Green;
#endif
	m_TileDroppers.push_back(Dropper);
}

bool CMatrixCode::IsOutOfBounds(sf::Vector2u Pos) const
{
	return Pos.x >= m_WindowLimits.x || Pos.y >= m_WindowLimits.y;
}
