#pragma once
#include "Tileset.hpp"
#include <memory>
#include <random>

#define MT_RANDOMCOLORS 0

namespace sf
{
	class Vector2m : public Vector2u
	{
	public:
		Vector2m() : Vector2u()
		{}

		Vector2m(Vector2u& other) : Vector2u(other)
		{}

		int GetHash() const
		{
			int Temp = (y + ((x + 1) / 2));
			return x + std::pow(Temp, 2);
		}

		bool operator <(const Vector2m& Other) const
		{
			return GetHash() < Other.GetHash();
		}
	};
}

struct TileDropper
{
	sf::Vector2m Position;
	unsigned nMaxLifeTime, nLifeTime;
	sf::Color TrailColor;
};

class CMatrixCode
{
public:
	CMatrixCode(CTileSet* pTileSet, sf::Vector2u WindowLimits);

	void Update();
	void Draw(sf::RenderWindow& Window) const;
private:
	const sf::Vector2u m_WindowLimits;
	std::unique_ptr<CTileSet> m_pTileSet;
	std::map<sf::Vector2m, std::shared_ptr<CTile>> m_ActiveTiles;
	std::vector<TileDropper> m_TileDroppers;
	unsigned long m_nFramesPassed;

	sf::Vector2u RandomPosition(const unsigned nMaxX, const unsigned nMaxY) const;
	unsigned RandomNumber(const unsigned nMin, const unsigned nMax) const;
	sf::Color RandomColor() const;

	void CreateTile(const TileDropper& Dropper);
	void CreateDropper(sf::Vector2u Pos, unsigned nLifeTime);
	bool IsOutOfBounds(sf::Vector2u Pos) const;
};