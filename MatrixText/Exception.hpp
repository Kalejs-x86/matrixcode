#pragma once
#include <exception>

#define CHECK(cond, emsg) if (!(cond)) throw std::exception(emsg);